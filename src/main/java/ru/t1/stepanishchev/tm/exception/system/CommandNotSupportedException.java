package ru.t1.stepanishchev.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command is not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Command '" + command + "' is not supported.");
    }

}